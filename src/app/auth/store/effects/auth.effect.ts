import { Injectable } from "@angular/core";
import { Effect, Actions } from "@ngrx/effects";
import { switchMap, map, catchError, tap, mergeMap } from "rxjs/operators";
import { of } from "rxjs/observable/of";
import { ServerAuth } from "../../model/auth.model";
import * as fromAppStore from "./../../../shared/store";
import * as authAction from "./../actions";
import * as loginAction from "./../../../login/store/actions";
import { UserCredentials } from "../../model/credentials.model";
import { Observable } from "rxjs";
@Injectable()
export class AuthEffect {
  constructor(public actions$: Actions) {}
  @Effect({ dispatch: true })
  auth$ = this.actions$.ofType(authAction.AuthActionTypes.Auth).pipe(
    map((log: authAction.Auth) => log.payload),
    switchMap((payload: UserCredentials) => {
      return this.fakeServer(payload).pipe(
        /*         
              trigger multiple actions   
              mergeMap(() => [
          new authAction.AuthSuccess({ token: "12345" }),
          new fromAppStore.Go({ path: ["/app"] })
        ]), */

        map((token: any) => {
          if (!!token) {
            return new authAction.AuthSuccess(token);
          } else {
            throw Error("creds faild");
          }
        }),
        catchError(err => of(new authAction.AuthFail(err)))
      );
    })
  );
  @Effect({ dispatch: true })
  authSuccess$ = this.actions$
    .ofType(authAction.AuthActionTypes.AuthSuccess)
    .pipe(
      map((log: authAction.AuthSuccess) => log.payload),
      map((token: ServerAuth) => new loginAction.LoginSuccess(token))
    );

  @Effect({ dispatch: true })
  authFail$ = this.actions$
    .ofType(authAction.AuthActionTypes.AuthFail)
    .pipe(map((err: any) => new loginAction.LoginFail(err)));

  @Effect({ dispatch: true })
  authLogout$ = this.actions$
    .ofType(authAction.AuthActionTypes.AuthLogout)
    .pipe(
      tap(() => console.log("logout ")),
      map((err: any) => new fromAppStore.Go({ path: ["/login"] }))
    );

  private fakeServer = (payload: UserCredentials): Observable<any> => {
    if (payload.username === "toto" || payload.password === "aa") {
      const token: string = Math.random()
        .toString(36)
        .substring(0, 7);
      return of(token);
    }
    return of(null);
  };
}
