import * as fromAuth from "./auth.reducers";
import { ActionReducerMap, createFeatureSelector, createSelector } from "@ngrx/store";
export interface AuthUserState {
  user: fromAuth.AuthState;
}
export const reducers: ActionReducerMap<AuthUserState> = {
  user: fromAuth.reducer
};
