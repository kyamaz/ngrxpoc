import { ServerAuth } from "../../model/auth.model";
import { Action } from "@ngrx/store";
import * as authAction from "./../actions/auth.actions";

export interface AuthState extends ServerAuth {}
export const initialState: ServerAuth = {
  token: undefined
};

export const reducer = (state = initialState, action: authAction.AuthAction): ServerAuth => {
  switch (action.type) {
    case authAction.AuthActionTypes.Auth: {
      return { ...state };
    }
    case authAction.AuthActionTypes.AuthSuccess: {
      const token = action.payload;
      //const { token } = action.payload;
      return { ...state, token };
    }
    case authAction.AuthActionTypes.AuthLogout:
    case authAction.AuthActionTypes.AuthFail: {
      console.log("auth failed");
      return { ...state, token: undefined };
    }

    default:
      return state;
  }
};
