import * as fromAuth from "./../reducers/auth.reducers";
import { createFeatureSelector, createSelector } from "@ngrx/store";
import { ServerAuth } from "../../model/auth.model";

export const getAuthState = createFeatureSelector<fromAuth.AuthState>("auth");
export const getToken = createSelector(getAuthState, (state: ServerAuth) => state.token);
