import { Action } from "@ngrx/store";
import { ServerAuth } from "./../../model/auth.model";
import { UserCredentials } from "../../model/credentials.model";

export enum AuthActionTypes {
  Auth = "[Auth] try auth",
  AuthSuccess = "[Auth] auth success",
  AuthFail = "[Auth] auth fails",
  AuthLogout = "[Auth] auth logout"
}
export class Auth implements Action {
  readonly type: string = AuthActionTypes.Auth;
  constructor(public payload: UserCredentials) {}
}
export class AuthSuccess implements Action {
  readonly type: string = AuthActionTypes.AuthSuccess;
  constructor(public payload: ServerAuth) {}
}
export class AuthFail implements Action {
  readonly type: string = AuthActionTypes.AuthFail;
  constructor(public payload: any) {}
}
export class AuthLogout implements Action {
  readonly type: string = AuthActionTypes.AuthLogout;
  constructor() {}
}

export type AuthAction = Auth | AuthSuccess | AuthFail;
