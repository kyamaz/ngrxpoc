import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { StoreModule } from "@ngrx/store";
import { reducers, AuthEffects } from "./store";
import { EffectsModule } from "@ngrx/effects";
@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature("auth", reducers),
    EffectsModule.forFeature(AuthEffects)
  ]
})
export class AuthModule {}
