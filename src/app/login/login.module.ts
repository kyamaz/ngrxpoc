import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { MatInputModule, MatButtonModule, MatCardModule, MatDialogModule } from "@angular/material";

import { LoginComponent } from "./components/login/login.component";
import { Routes, RouterModule } from "@angular/router";
import { StoreModule } from "@ngrx/store";
import { reducers, effects } from "./store";
import { EffectsModule } from "@ngrx/effects";
import { LoginFormComponent } from "./components/login-form/login-form.component";
const ROUTES: Routes = [{ path: "", component: LoginComponent }];

import { SharedModule } from "./../shared/shared.module";

//directives

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    RouterModule.forChild(ROUTES),
    StoreModule.forFeature("login", reducers),
    EffectsModule.forFeature(effects)
  ],
  declarations: [LoginComponent, LoginFormComponent]
})
export class LoginModule {}
