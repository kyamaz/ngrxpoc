import * as fromLoginForm from "./../reducers/login-form.reducers";
import { createFeatureSelector, createSelector } from "@ngrx/store";

export const getForm = createFeatureSelector<fromLoginForm.FormLoginState>("form");
/* export const getLoginState = createSelector(getLogin, (login: any) => login.state);
export const getPendingState = createSelector(getLoginState, (state: any) => state.pending);
export const getErrorState = createSelector(getLoginState, (state: any) => state.error); */
