import * as fromAuth from "./../reducers/login.reducers";
import { createFeatureSelector, createSelector } from "@ngrx/store";

export const getLogin = createFeatureSelector<fromAuth.LoginState>("login");
export const getLoginState = createSelector(getLogin, (login: any) => login.state);
export const getPendingState = createSelector(getLoginState, (state: any) => state.pending);
export const getErrorState = createSelector(getLoginState, (state: any) => state.error);
