import * as fromLogin from "./login.reducers";
import * as fromFormLogin from "./login-form.reducers";

import { ActionReducerMap, createFeatureSelector, createSelector } from "@ngrx/store";
export interface LoginState {
  state: fromLogin.LoginState;
  form: fromFormLogin.FormLoginState;
}
export const reducers: ActionReducerMap<LoginState> = {
  state: fromLogin.reducer,
  form: fromFormLogin.reducer
};
