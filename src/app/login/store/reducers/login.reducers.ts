import { Action } from "@ngrx/store";
import * as fromAuth from "./../actions/";
import { UserCredentials } from "../../../auth/model/credentials.model";
export interface LoginState {
  pending: boolean;
  error: any | null;
}

export const initialState: LoginState = {
  pending: false,
  error: null
};

export const reducer = (state = initialState, action: fromAuth.loginAction): LoginState => {
  switch (action.type) {
    case fromAuth.LoginActionTypes.Login: {
      return { ...state, pending: true };
    }
    case fromAuth.LoginActionTypes.LoginSuccess: {
      return { ...state, pending: false, error: null };
    }
    case fromAuth.LoginActionTypes.LoginFail: {
      return { ...state, pending: false, error: "has error" };
    }
    case fromAuth.LoginActionTypes.Logout: {
      return { ...state, pending: false, error: null };
    }
    default:
      return state;
  }
};
