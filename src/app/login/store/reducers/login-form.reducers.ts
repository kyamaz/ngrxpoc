import { Action } from "@ngrx/store";
import * as fromAuth from "./../actions/";
import { UserCredentials } from "../../../auth/model/credentials.model";
export interface FormLoginState {
  username: string;
  password: string;
}

export const initialState: FormLoginState = {
  username: undefined,
  password: undefined
};

export const reducer = (state = initialState, action: fromAuth.LoginFormAction): FormLoginState => {
  switch (action.type) {
    case fromAuth.LoginFormActionTypes.Form: {
      return { ...state };
    }
    case fromAuth.LoginFormActionTypes.FormUpdate: {
      const { username, password } = action.payload;

      console.log({ ...state, username, password });
      return { ...state, username, password };
    }

    default:
      return state;
  }
};
