import { Injectable } from "@angular/core";
import { Effect, Actions } from "@ngrx/effects";
import { switchMap, map, catchError, tap, mergeMap } from "rxjs/operators";
import { of } from "rxjs/observable/of";
import { LoginActionTypes, Login, LoginFail, LoginSuccess, loginAction } from "./../actions";
import * as fromAuthAction from "./../../../auth/store/actions";
import { UserCredentials } from "../../../auth/model/credentials.model";
import * as fromAppStore from "./../../../shared/store";
@Injectable()
export class LoginEffect {
  constructor(public actions$: Actions) {}
  @Effect({ dispatch: true })
  login$ = this.actions$
    .ofType(LoginActionTypes.Login)
    .pipe(
      map((log: Login) => log.payload),
      map((creds: UserCredentials) => new fromAuthAction.Auth(creds))
    );
  @Effect({ dispatch: true })
  loginSuccess$ = this.actions$
    .ofType(LoginActionTypes.LoginSuccess)
    .pipe(
      map((log: LoginSuccess) => log.payload),
      map(() => new fromAppStore.Go({ path: ["/app"] }))
    );

  // Ro whorever reading, do not effect fail as it triggers a loop where effect call himself
  @Effect({ dispatch: true })
  loginOut$ = this.actions$
    .ofType(LoginActionTypes.Logout)
    .pipe(map(() => new fromAuthAction.AuthLogout()));
}
