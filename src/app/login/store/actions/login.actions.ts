import { Action } from "@ngrx/store";

import { ServerAuth } from "./../../../auth/model/auth.model";
import { UserCredentials } from "../../../auth/model/credentials.model";
export enum LoginActionTypes {
  Login = "[Login Page] Login",
  LoginSuccess = "[Login Page] Login success",
  LoginFail = "[Login Page] Login fails",
  Logout = "[Login Page] Logout"
}

export class Login implements Action {
  readonly type: string = LoginActionTypes.Login;
  constructor(public payload: UserCredentials) {}
}
export class LoginSuccess implements Action {
  readonly type: string = LoginActionTypes.LoginSuccess;
  constructor(public payload: ServerAuth) {}
}
export class LoginFail implements Action {
  readonly type: string = LoginActionTypes.LoginFail;
  constructor(public payload: any) {}
}
export class Logout implements Action {
  readonly type: string = LoginActionTypes.Logout;
  constructor() {}
}

export type loginAction = Login | LoginSuccess | LoginSuccess | Logout;
