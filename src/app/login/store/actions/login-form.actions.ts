import { Action } from "@ngrx/store";

import { UserCredentials } from "../../../auth/model/credentials.model";
export enum LoginFormActionTypes {
  Form = "[Login form] form",
  FormUpdate = "Login form] Login form update"
}

export class Form implements Action {
  readonly type: string = LoginFormActionTypes.Form;
  constructor(public payload: UserCredentials) {}
}
export class FormUpdate implements Action {
  readonly type: string = LoginFormActionTypes.FormUpdate;
  constructor(public payload: UserCredentials) {}
}

export type LoginFormAction = Form | FormUpdate;
