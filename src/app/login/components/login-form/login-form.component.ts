import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";
import { UserCredentials } from "../../../auth/model/credentials.model";

@Component({
  selector: "login-form",
  templateUrl: "./login-form.component.html",
  styleUrls: ["./login-form.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginFormComponent implements OnInit {
  @Input() error: string;
  @Input() pending: string;
  @Input() data: any;
  @Output() creds = new EventEmitter<UserCredentials>();
  @Output() updateForm = new EventEmitter<UserCredentials>();

  public form: FormGroup;

  constructor(private _fb: FormBuilder) {
    console.log(this.data);
  }

  ngOnInit() {
    this.form = this._fb.group({
      username: ["", Validators.compose([Validators.required, Validators.minLength(2)])],
      password: ["", Validators.compose([Validators.required, Validators.minLength(2)])]
    });

    this.form.valueChanges.subscribe(f => {
      this.updateForm.emit(f);
    });
  }

  onSubmit() {
    this.creds.emit(this.form.value);
  }
}
