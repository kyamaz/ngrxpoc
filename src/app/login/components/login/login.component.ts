import { Component, OnInit } from "@angular/core";
import { Router, NavigationStart } from "@angular/router";
import * as fromLoginStore from "./../../store";
import { Store } from "@ngrx/store";
import { Login, FormUpdate } from "../../store/actions";
import { Observable } from "rxjs";
import { UserCredentials } from "./../../../auth/model/credentials.model";
import { FormGroup, FormBuilder } from "@angular/forms";
import { of } from "rxjs/observable/of";

@Component({
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  //form: FormGroup;
  pending$: Observable<boolean>;
  error$: Observable<boolean>;
  form$: Observable<any>;
  constructor(
    private _router: Router,
    private _store: Store<fromLoginStore.LoginState>,
    public _fb: FormBuilder
  ) {}
  ngOnInit() {
    /*     this.form = this._fb.group({
      username: ["", Validators.compose([Validators.required, Validators.minLength(2)])],
      password: ["", Validators.compose([Validators.required, Validators.minLength(2)])]
    }); */
    this.pending$ = this._store.select(fromLoginStore.getPendingState);
    this.error$ = this._store.select(fromLoginStore.getErrorState);
    this.form$ = this._store.select(fromLoginStore.getLogin);
  }

  login(e) {
    this._store.dispatch(new Login(e));
  }

  update(e) {
    this._store.dispatch(new FormUpdate(e));
    console.log(e);
    // this._store.dispatch( new )
  }
}
