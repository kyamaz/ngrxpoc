import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DashComponent } from "./dash/dash.component";

import {
  MatGridListModule,
  MatCardModule,
  MatMenuModule,
  MatIconModule,
  MatButtonModule
} from "@angular/material";
import { environment } from "../environments/environment";

// store
import { StoreModule, MetaReducer } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { reducers, CustormSerializer } from "./shared/store/reducers";
import { StoreRouterConnectingModule, RouterStateSerializer } from "@ngrx/router-store";
import * as fromAppGuards from "./shared/guards";
import { effects } from "./shared/store";
// shared
import { SharedModule } from "./shared/shared.module";
//dev tools
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { storeFreeze } from "ngrx-store-freeze";
// routes

const ROUTES: Routes = [
  {
    path: "login",
    loadChildren: "./login/login.module#LoginModule"
  },
  {
    path: "app",
    canActivate: [...fromAppGuards.guards],
    component: DashComponent
  },
  { path: "**", redirectTo: "login" }
];

export const metaReducers: MetaReducer<any>[] = !environment.production ? [storeFreeze] : [];

@NgModule({
  declarations: [AppComponent, DashComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    SharedModule,
    RouterModule.forRoot(ROUTES),
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot([...effects]),
    StoreRouterConnectingModule,
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  providers: [
    { provide: RouterStateSerializer, useClass: CustormSerializer },
    ...fromAppGuards.guards
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
