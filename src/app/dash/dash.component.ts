import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import * as fromAuth from "./../auth/store/actions";

@Component({
  selector: "dash",
  templateUrl: "./dash.component.html",
  styleUrls: ["./dash.component.scss"]
})
export class DashComponent {
  form$;
  constructor(private _store: Store<any>) {}

  logout() {
    this._store.dispatch(new fromAuth.AuthLogout());
  }
}
