import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Observable";
import { tap, filter, take, switchMap, catchError, map, flatMap } from "rxjs/operators";
import * as fromStore from "./../../shared/store/";
import * as fromAuthSelectors from "./../../auth/store/selectors";
import { of } from "rxjs/observable/of";
import { AuthState } from "../../auth/store/reducers/auth.reducers";
import { AuthUserState } from "../../auth/store";
import * as fromAuthAction from "./../../auth/store/actions";

@Injectable()
export class AppAuthGuard implements CanActivate {
  constructor(private _store: Store<any>, private _router: Router) {}
  canActivate(): Observable<boolean> {
    //flatmap to handle observable from store then servre response
    return this._store.select(fromStore.getAuthUserState).pipe(
      flatMap((authState: AuthState) => {
        if (!!authState) {
          // check for store
          const token = authState.token;
          console.log("auth in store");

          if (!!token) {
            return this.fakeServerCheckToken(token);
          }
          console.log("auth in browser");

          // return this.checkInBrowser();

          // check for local storage ot cookie
        }
        console.log("auth failed");

        //auth failed
        new fromAuthAction.AuthFail("token not valid ");
        this._router.navigate(["./login"]);
        return of(false);
      })
    );
  }
  fakeServerCheckToken(token: string): Observable<boolean> {
    return of(token.length === 7);
  }

  checkInBrowser(): Observable<boolean> {
    const tokenFromBrowser = "1234567";
    return this.fakeServerCheckToken(tokenFromBrowser);
  }
}
