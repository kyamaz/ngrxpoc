import { AppAuthGuard } from "./app.guard";

export const guards: any[] = [AppAuthGuard];

export * from "./app.guard";
