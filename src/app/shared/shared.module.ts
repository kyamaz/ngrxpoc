import { AuthModule } from "./../auth/auth.module";
import { NgModule } from "@angular/core";

@NgModule({
  imports: [AuthModule],
  providers: []
})
export class SharedModule {}
