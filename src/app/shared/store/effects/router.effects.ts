import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Location } from "@angular/common";
import { Actions, Effect } from "@ngrx/effects";
import * as RouterActions from "./../actions";
import { tap, map } from "rxjs/operators";
import * as routerAction from "../actions/router.actions";
import { RouterAction } from "@ngrx/router-store";

@Injectable()
export class RouterEffects {
  constructor(private _actions: Actions, private _router: Router, private _location: Location) {}
  @Effect({
    dispatch: false
  })
  navigate$ = this._actions.ofType(RouterActions.NavActionTypes.Go).pipe(
    map((action: RouterActions.Go) => action.paylod),
    tap(({ path, query: queryParams, extras }) => {
      this._router.navigate(path, { queryParams, ...extras });
    })
  );
  @Effect({ dispatch: false })
  navigateBack$ = this._actions
    .ofType(RouterActions.NavActionTypes.Back)
    .pipe(tap(_ => this._location.back()));

  @Effect({ dispatch: false })
  navigateForward$ = this._actions
    .ofType(RouterActions.NavActionTypes.Forward)
    .pipe(tap(_ => this._location.forward()));
}
