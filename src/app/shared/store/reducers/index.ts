import * as fromRouter from "@ngrx/router-store";
import { ActivatedRouteSnapshot, Params, RouterStateSnapshot } from "@angular/router";
import { ActionReducerMap, createFeatureSelector, createSelector } from "@ngrx/store";
import * as fromAuthReducers from "./../../../auth/store/reducers";
import { AuthState } from "../../../auth/store/reducers/auth.reducers";
export interface RouterStateUrl {
  url: string;
  queryParams: Params;
  params: Params;
}
export interface State {
  routerReducer: fromRouter.RouterReducerState<RouterStateUrl>;
  auth: any; //fromAuthReducers.AuthUserState
}
export const reducers: ActionReducerMap<State> = {
  routerReducer: fromRouter.routerReducer,
  auth: fromAuthReducers.reducers.user
};

export const getRouterState = createFeatureSelector<fromRouter.RouterReducerState<RouterStateUrl>>(
  "router"
);

export const getAuthState = createFeatureSelector<fromAuthReducers.AuthUserState>("auth");
export const getAuthUserState = createSelector(
  getAuthState,
  (state: fromAuthReducers.AuthUserState) => state.user
);

export class CustormSerializer implements fromRouter.RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    const { url } = routerState;
    const { queryParams } = routerState.root;
    let state: ActivatedRouteSnapshot = routerState.root;
    while (state.firstChild) {
      state = state.firstChild;
    }
    const { params } = state;
    return {
      url,
      queryParams,
      params
    };
  }
}
