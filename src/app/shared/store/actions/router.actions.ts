import { Action } from "@ngrx/store";
import { NavigationExtras } from "@angular/router";
import { query } from "@angular/animations";

export enum NavActionTypes {
  Go = "[Router] Go",
  Back = "[Router] Back",
  Forward = "[Router] Forward"
}

export class Go implements Action {
  readonly type = NavActionTypes.Go;
  constructor(
    public paylod: {
      path: any[];
      query?: object;
      extras?: NavigationExtras;
    }
  ) {}
}
export class Back implements Action {
  readonly type = NavActionTypes.Back;
}
export class Forward implements Action {
  readonly type = NavActionTypes.Forward;
}

export type Actions = Go | Forward | Back;
